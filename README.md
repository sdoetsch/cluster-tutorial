# PSFTheory Cluster Tutorial

This repository is in intended to be a collection of documentation, tutorials
and scripts for everything regarding usage of the MPCDF clusters (isaac, cobra, ...)
as well as usage of simulation codes (PLUTO, Pencil, etc).

If you have anything to add/correct/amend, just fork this repository, make your
changes and make a pull request, your changes then will be merged in.

In the future this repository can be hosted as a webpage, or a continous pdf,
or migrated into a wiki.

## Table of Contents
 - [ISAAC - Pencil guide for beginners](Isaac_PENCIL_guide_for_beginners.txt)

## Planned Tutorials
 - SSH configuration (via gate as proxy)
 - SLURM introduction
 - bash / GNU screen introduction (for long running sessions)
 - Plutocode introduction
 - Python environments (venv, conda) & Jupyter Notebook/Lab (with SSH forwarding)
